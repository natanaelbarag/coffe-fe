import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderCoffeComponent } from './order-coffe/order-coffe.component';
import { CoffeTypesComponent } from './coffe-types/coffe-types.component';


const routes: Routes = [
  { path: 'orderCoffe' , component: OrderCoffeComponent},
  { path: 'coffeTypes' , component: CoffeTypesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
