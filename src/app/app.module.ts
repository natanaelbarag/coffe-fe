import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { OrderCoffeComponent } from "./order-coffe/order-coffe.component";
import { CoffeTypesComponent } from "./coffe-types/coffe-types.component";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, OrderCoffeComponent, CoffeTypesComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [CoffeTypesComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
