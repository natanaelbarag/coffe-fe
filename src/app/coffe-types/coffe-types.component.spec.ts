import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeTypesComponent } from './coffe-types.component';

describe('CoffeTypesComponent', () => {
  let component: CoffeTypesComponent;
  let fixture: ComponentFixture<CoffeTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffeTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffeTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
