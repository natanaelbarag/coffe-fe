import { Component, OnInit } from '@angular/core';
import { CoffeTypeService } from '../services/coffe-type-service/coffe-type.service';
import { CoffeType } from '../models/coffetype';

@Component({
  selector: 'app-coffe-types',
  templateUrl: './coffe-types.component.html',
  styleUrls: ['./coffe-types.component.css']
})
export class CoffeTypesComponent implements OnInit {

  public coffeTypes: CoffeType[];
  coffeTypeEdit : CoffeType;
  addCoffeType : CoffeType = {
    id : null,
    name: '', 
    discount : 0 , 
    quantity : 0,
    price: 0
  };

  constructor(public coffeTypeService : CoffeTypeService) { }

  ngOnInit() {
    this.coffeTypeService.getAllCoffeTypes().subscribe(
       data => this.coffeTypes = data
    )
  }


  deleteCoffeType(id: number){
    this.coffeTypes = this.coffeTypes.filter(el => el.id != id);
    this.coffeTypeService.delete(id).subscribe();
  }

  setEditCoffeType(coffeType: CoffeType){
    this.coffeTypeEdit = coffeType;
  }

  updateCoffeType(name, discount, quantity, price){
    this.setCoffeTypeProperties(this.coffeTypeEdit, name, discount,quantity,price);
    this.coffeTypeService.update(this.coffeTypeEdit).subscribe();
  }

  saveCoffeType(name, discount, quantity, price){
    this.setCoffeTypeProperties(this.addCoffeType, name, discount, quantity, price);
    this.coffeTypeService.saveCoffeType(this.addCoffeType).subscribe(added => this.coffeTypes.push(added));
  
  }

  setCoffeTypeProperties(coffeType: CoffeType, name, discount, quantity, price){
    coffeType.price = price;
    coffeType.name = name;
    coffeType.discount = discount;
    coffeType.quantity = quantity;
  }
}
