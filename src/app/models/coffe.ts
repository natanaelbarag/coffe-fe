import { Country } from "./country";
import { CoffeType } from "./coffetype";

export interface Coffe {
  id: number;
  name: string;
  coffeType: CoffeType;
  country: string;
  price: number;
  quantity: number;
}
