export interface CoffeType {
  id: number;
  name: string;
  discount: number;
  quantity: number;
  price: number;
}
