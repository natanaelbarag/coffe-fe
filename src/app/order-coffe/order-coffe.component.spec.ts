import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { OrderCoffeComponent } from "./order-coffe.component";

describe("OrderCoffeComponent", () => {
  let component: OrderCoffeComponent;
  let fixture: ComponentFixture<OrderCoffeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderCoffeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCoffeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
