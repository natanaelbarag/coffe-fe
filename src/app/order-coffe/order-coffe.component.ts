import { Component, OnInit } from "@angular/core";
import { OrderCoffeService } from "../services/order-coffe-service/order-coffe.service";
import { Coffe } from "../models/coffe";
import { CoffeTypeService } from "../services/coffe-type-service/coffe-type.service";
import { CoffeType } from "../models/coffetype";
import { Country } from "../models/country";

@Component({
  selector: "app-order-coffe",
  templateUrl: "./order-coffe.component.html",
  styleUrls: ["./order-coffe.component.css"],
})
export class OrderCoffeComponent implements OnInit {
  coffeOrders: Coffe[];
  public coffeTypes: CoffeType[];

  countries = Object.keys(Country).slice(Object.keys(Country).length / 2);

  constructor(
    public orderCoffeService: OrderCoffeService,
    public coffeTypeService: CoffeTypeService
  ) {}

  ngOnInit() {
    this.orderCoffeService.getAllCoffeOrders().subscribe((data) => {
      this.coffeOrders = data;
    });
    this.coffeTypeService
      .getAllCoffeTypes()
      .subscribe((data) => (this.coffeTypes = data));
  }

  orderCoffe(name, coffeTypeId, country, quantity) {
    coffeTypeId = coffeTypeId.slice(0, 1);
    let coffeType: CoffeType = this.coffeTypes
      .filter((el) => el.id == coffeTypeId)
      .pop();
    let coffe: Coffe = this.buildOrderCoffeObject(
      name,
      coffeType,
      country,
      quantity
    );
    this.orderCoffeService
      .orderCoffe(coffe)
      .subscribe((added) => this.coffeOrders.push(added));
  }

  deleteCoffeOrder(coffe: Coffe) {
    this.coffeOrders = this.coffeOrders.filter((el) => el.id != coffe.id);
    this.orderCoffeService.deleteCoffeOrder(coffe).subscribe();
  }

  private buildOrderCoffeObject(name, coffeType, country, quantity): Coffe {
    return {
      id: null,
      coffeType: coffeType,
      quantity: quantity,
      price: null,
      name: name,
      country: country,
    };
  }
}
