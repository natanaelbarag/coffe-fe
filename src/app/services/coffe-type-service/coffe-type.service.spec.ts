import { TestBed } from '@angular/core/testing';

import { CoffeTypeService } from './coffe-type.service';

describe('CoffeTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoffeTypeService = TestBed.get(CoffeTypeService);
    expect(service).toBeTruthy();
  });
});
