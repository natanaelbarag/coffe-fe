import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoffeType } from "src/app/models/coffetype";
import { Observable } from "rxjs";
import { httpOptions } from '../httpOptions';


@Injectable({
  providedIn: "root",
})
export class CoffeTypeService {
  constructor(private http: HttpClient) {}

  coffeTypesURL: string = "http://localhost:8089/coffetype/all";
  coffeTypeUrl: string = "http://localhost:8089/coffetype";

  getAllCoffeTypes(): Observable<CoffeType[]> {
    return this.http.get<CoffeType[]>(this.coffeTypesURL);
  }

  delete(id: number): Observable<CoffeType> {
    return this.http.delete<CoffeType>(
      this.coffeTypeUrl + "/" + id,
      httpOptions
    );
  }

  update(coffeTypeEdit: CoffeType): Observable<CoffeType> {
    return this.http.put<CoffeType>(
      this.coffeTypeUrl,
      coffeTypeEdit,
      httpOptions
    );
  }

  saveCoffeType(addCoffeType: CoffeType) {
    return this.http.post<CoffeType>(
      this.coffeTypeUrl,
      addCoffeType,
      httpOptions
    );
  }
}
