import { TestBed } from '@angular/core/testing';

import { OrderCoffeService } from './order-coffe.service';

describe('OrderCoffeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderCoffeService = TestBed.get(OrderCoffeService);
    expect(service).toBeTruthy();
  });
});
