import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Coffe } from "src/app/models/coffe";
import { httpOptions } from '../httpOptions';

@Injectable({
  providedIn: "root",
})
export class OrderCoffeService {
  constructor(private http: HttpClient) {}

  coffeOrdersURL: string = "http://localhost:8089/coffe/allCoffeOrders";
  coffeUrl: string = "http://localhost:8089/coffe";

  getAllCoffeOrders(): Observable<Coffe[]> {
    return this.http.get<Coffe[]>(this.coffeOrdersURL);
  }

  deleteCoffeOrder(coffe: Coffe): Observable<Coffe> {
    return this.http.delete<Coffe>(this.coffeUrl + "/" + coffe.id, httpOptions);
  }

  orderCoffe(coffe: any) {
    return this.http.post<Coffe>(this.coffeUrl, coffe, httpOptions);
  }
}
